# An IR remote receiver with relay

The purpose of this page is to explain step by step the realization of an infrared remote receiver based on ARDUINO NANO.

The board uses the following components :

 * an ARDUINO NANO
 * a relay module
 * a TSOP4838
 * some connectors

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/10/telecommande-de-relais-par-infra-rouge.html

