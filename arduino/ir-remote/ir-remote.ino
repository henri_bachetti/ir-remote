
#include <IRremote.h>

#define RECV_PIN    3
#define RELAY_PIN   4
#define RELAY_TYPE  LOW       // for a low level relay
//#define RELAY_TYPE  HIGH    // for a high level relay

IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  Serial.begin(115200);
  Serial.println("IR remote");
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, RELAY_TYPE == HIGH ? LOW : HIGH);
  irrecv.enableIRIn();
}

void loop()
{
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    if (results.value == 0x619EC03F) {
      int actual = digitalRead(RELAY_PIN);
      Serial.print(actual == RELAY_TYPE ? "Turn OFF" : "Turn ON"); Serial.println(" the relay");
      digitalWrite(RELAY_PIN, !actual);
    }
    irrecv.resume();
  }
  delay(100);
}
